#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 15:39:12 2021

@author: chenhaowu
"""

from source import *
import pandas as pd
import matplotlib.pyplot as plt



risk_df = pd.read_csv(risk_data_path)
pricing_df = pd.read_csv(pricing_data_path)

"""
 Plot a graph of click through rate against position using the pricing data set.
"""
position_click_data = pricing_df.groupby(["Position", "ClickFlag"]).size().reset_index(name="Times")
position_clicked_times = position_click_data.loc[position_click_data['ClickFlag'] == True]

position_clicked_times.plot(x='Position', y='Times')

"""
1. Is this behaviour what you would expect? If not why not?

As shown in the plot and the printed data frame below the lower or stating position (1) 
has the large amount of click times, but the last position (20) has the least clicks, and the click
rates dropped with the position number increases.  

Depends on the content of each position, if the earlier positions (1 and 2) have given enough
information to the customer then this could be the behaviour I expected as only small amount of 
the customer may look into niche information in later position content.

However, if the last position is content, it is important information. For example for an online supermarket 
the last few positions could be related with 'payment confirmation', the conversion rates are with low
in this case and it won't be as expected as we would hope there are more customers be able to reach the
last step of the user journey.


print(position_clicked_times)
    Position  ClickFlag  Times
          1       True    423
          2       True    160
          3       True     71
          4       True     46
          5       True     25
          6       True     16
          7       True     11
          8       True     13
          9       True     13
         10       True      5
         11       True      4
         12       True      3
         13       True      3
         14       True      4
         15       True      2
         16       True      6
         17       True      4
         19       True      3
         20       True      1

2. Position clearly has a large effect on click through rate, suggest what an insurance brand could do to improve their performance?
What are the trade-offs between improving performance and profit?
What data do you think is missing from the given data set? What could have been gathered that you would like to see?
"""


#merged = risk.merge(pricing, on='QuoteID')
#
#pricing_data_size = len(set(pricing['QuoteID']))
#risk_data_size = len(set(risk['QuoteID']))
#merged_data_size = len(set(merged['QuoteID']))
