#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 15:32:13 2021

@author: chenhaowu
"""

from pathlib import Path
import os

THIS_DIR = Path(__file__).parent
risk_data_path = THIS_DIR.parent / 'DataFiles/risk.csv'
pricing_data_path =THIS_DIR.parent / 'DataFiles/pricing.csv'

def check_folder_empty():
    if len(os.listdir(THIS_DIR.parent / 'DataFiles') ) == 0:
        print("Directory is empty")
    else:    
        print("Directory is not empty")

def check_csv_readable(PATH):
    if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
        print("File exists and is readable")
    else:
        print("Either the file is missing or not readable")