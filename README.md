# Data Analysis Exercise

## Code

Your first task will be to create a branch to work from and then follow the instructions below.

The sprint should last 3-4 hours, but you can complete it earlier if you wish. 
It ends once you issue your merge request.

Please make commits as you go (> 1 and < 100) and provide any necessary instructions.

## Introduction

The idea of this exercise is try out some of the problems and tasks that we do at ctrlio. For these tasks, feel free to use any language / software you are comfortable with or to give written answers, whichever you think is appropriate.

The data and explanations given here are not sufficient to answer the questions and explain everything. Please ask us questions! 

Attached are two data sets. Both are gathered from customers using a price comparison website to find insurance. The first data set is some user information about the kind of insurance cover they are looking for. The second data set is a list of the prices by brand that each customer was shown.  They are linked by QuoteID.

Answer the questions below and where you think appropriate detail your reasoning, methodology and caveats.

### Part 1 - Customer Behaviour  

Click through rate is the ratio of clicks to impressions. Impressions is the number of times a quote was displayed. Click through rate is a key performance indicator that we can use to investigate customer behaviour. Plot a graph of click through rate against position using the pricing data set. 

1. Is this behaviour what you would expect? If not why not?
2. Position clearly has a large effect on click through rate, suggest what an insurance brand could do to improve their performance?
3. What are the trade-offs between improving performance and profit?
4. What data do you think is missing from the given data set? What could have been gathered that you would like to see? 

### Part 2 - Insurer Behaviour

Someone has told you that the price of insurance is higher when you search closer to your renewal date. Using both data sets, investigate this claim, and produce evidence that either supports or opposes it. Consider the following while you complete this task:

1. What explanations can you think of that could account for this observation?
2. Why might insurers base their price on time to renewal?
3. How does customer behaviour change with respect to time to renewal?
